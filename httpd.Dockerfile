FROM docker.io/httpd:2.4.54-alpine

LABEL maintainer="u@xaked.com"\
      app="httpd"\
      version="2.4.54"

COPY --chown=root:root httpd.conf /usr/local/apache2/conf/httpd.conf
COPY --chown=root:root entrypoint.sh /entrypoint.sh
COPY --chown=www-data:www-data html/ /var/www/html/

RUN chmod 700 /entrypoint.sh\
 && mkdir -p /usr/local/apache2/conf/sites-enabled/

EXPOSE 80/tcp 8080/tcp
CMD ["/entrypoint.sh"]
