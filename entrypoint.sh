#!/bin/ash

set -euo pipefail;

echo " > Checking for BASIC AUTH credentials in env...";
echo " > Looking for BASIC_AUTH_LOGIN and BASIC_AUTH_PASSWORD";
LOGIN=${BASIC_AUTH_LOGIN:-};
PASSWORD=${BASIC_AUTH_PASSWORD:-};
HTPASSWD="/usr/local/apache2/conf/.htpasswd";
SITE_CONFIG="/usr/local/apache2/conf/sites-enabled/000-default.conf";
SITE_CONFIG_8080="/usr/local/apache2/conf/sites-enabled/001-default.conf";

if [ -n "${LOGIN}" ] && [ -n "${PASSWORD}" ]; then
  echo " > Found LOGIN/PASS set. Adding user ${LOGIN}";
  echo "${PASSWORD}" | htpasswd -ic "${HTPASSWD}" "${LOGIN}" 2>/dev/null 1>/dev/null;
fi

echo " > Generating default virutal host config ${SITE_CONFIG}..."
cat <<EOF > "${SITE_CONFIG}"
<VirtualHost *:80>
    ServerAdmin u@xaked.com
    DocumentRoot /var/www/html/port-80
    ErrorLog /proc/1/fd/2
    CustomLog /proc/1/fd/1 combined

    <Location /check>
        ErrorDocument 200 "OK"
        RewriteEngine On
        RewriteRule .* - [R=200]
        Require all granted
    </Location>

    <Directory "/var/www/html/port-80">
EOF
if [ -n "${LOGIN}" ] && [ -n "${PASSWORD}" ]; then
    cat <<EOF >> "${SITE_CONFIG}"
        AuthType Basic
        AuthName "Auth Required"
        AuthUserFile /usr/local/apache2/conf/.htpasswd
        Require valid-user
EOF
else
    cat <<EOF >> "${SITE_CONFIG}"
        Require all granted
EOF
fi
cat <<EOF >> "${SITE_CONFIG}"
    </Directory>
</VirtualHost>
EOF

echo " > Generating default virutal host config ${SITE_CONFIG_8080}..."
cat <<EOF > "${SITE_CONFIG_8080}"
<VirtualHost *:8080>
    ServerAdmin u@xaked.com
    DocumentRoot /var/www/html/port-8080
    ErrorLog /proc/1/fd/2
    CustomLog /proc/1/fd/1 combined

    <Location /check>
        ErrorDocument 200 "OK"
        RewriteEngine On
        RewriteRule .* - [R=200]
        Require all granted
    </Location>

    <Directory "/var/www/html/port-8080">
EOF
if [ -n "${LOGIN}" ] && [ -n "${PASSWORD}" ]; then
    cat <<EOF >> "${SITE_CONFIG_8080}"
        AuthType Basic
        AuthName "Auth Required"
        AuthUserFile /usr/local/apache2/conf/.htpasswd
        Require valid-user
EOF
else
    cat <<EOF >> "${SITE_CONFIG_8080}"
        Require all granted
EOF
fi
cat <<EOF >> "${SITE_CONFIG_8080}"
    </Directory>
</VirtualHost>
EOF

echo " > Starting httpd server...";
httpd-foreground;
